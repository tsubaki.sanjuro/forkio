$(document).ready(function () {
	$(".logo").mouseenter(function () {
		$(".logo__h3").css("color", "#fff");
		$(".logo__img")[0].src = "dist/img/logo-hover.png"
	});

	$(".logo").mouseleave(function () {
		$(".logo__h3").css("color", "#8d81ac");
		$(".logo__img")[0].src = "dist/img/Logo.png";
	});

	$('.logo').click(function () {
		$(".nav__menu").fadeToggle(200);
	})

});
